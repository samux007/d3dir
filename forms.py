from django import forms
from django.forms import widgets


class Directory(forms.Form):
    spath = forms.CharField(max_length=200, required=True, label="")
