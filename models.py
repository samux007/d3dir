from django.db import models

class Config(models.Model):
    userid = models.DecimalField(max_digits=5, decimal_places=0)
    key    = models.CharField(max_length=300)
    value  = models.CharField(max_length=300)

class Directory(models.Model):
    name      = models.CharField(max_length=512)
    size      = models.DecimalField(max_digits=20,decimal_places=0)
    totalsize = models.DecimalField(max_digits=30,decimal_places=0)
    level     = models.DecimalField(max_digits=3, decimal_places=0)
    timestamp = models.DateTimeField('data estrazione')
